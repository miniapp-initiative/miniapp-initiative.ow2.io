---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Academia challenges
subtitle: How to participate
author: Christian Paterson
section: Join In
date: 2023-05-15
comments: false
draft: false
---

Huawei has kindly invited the QAI community to propose ideas for *co-animated* challenges. Below we describe how this works.

<!--more-->

## The principle

- Challenge ideas should relate to MiniApps.
- The proposer should be a [registered](https://www.ow2.org/view/QuickApp/Participants_Form) QAI participant.

Because these ideas come from _you_, a valued member of our community, when you propose a challenge idea you agree to:
1. be publicly associated to the challenge as a “co-organiser”;
2. help write the challenge description and goals so that they are interesting for all and within the realms of achievable;
3. join calls with student teams and provide reasonable support to any questions raised (remembering that the challenge is for the students not you).

**You also agree with the _nota bene_ shown in the side note**

Whilst it should be pretty obvious, just to be clear, challenge outputs will be published as open source/creative commons via the OW2 Quick App Initiative and/or other public code forge as appropriate.

>**_Nota bene_**
>
>Huawei has opened a contract with Telanto in order to organise challenges and wishes to share the possibility to create MiniApp related challenges on the Telanto platform with QAI registered participants under the following conditions:
>- Huawei remains the relationship owner with Telanto and, in its sole discretion, has a final say on which challenges can be submitted to the platform and which QAI participants may be granted/rescinded platform access.
>- Huawei retains oversight of all challenges.
>- All patents, copyrights, trademarks and challenge deliverables remain with their respective owners.
>- Telanto requires that platform users must not contact establishments or students beyond the scope of any particular challenge.
>- Huawei will not accept any liability for the abuse and/or misuse of the Telanto platform by the respective QAI registered participants.
>- All written content should be delivered under a [Creative Commons By 4.0](https://creativecommons.org/licenses/by/4.0/) license.
>- All software code content should be delivered under an approved [OSI open source license](https://opensource.org/licenses). The [Apache License, Version 2.0](https://opensource.org/licenses/Apache-2.0) is preferred.


## Participation steps

1. You can propose a challenges via the [QAI public mailing list](mailto:quickapp@ow2.org?subject=QAI%20academia%20challenge%20proposal), by email to the [QAI core team](mailto:quickapp-team@ow2.org?subject=QAI%20academia%20challenge%20proposal), as an issue on [GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/issues), or even a chat message using [Rocket.Chat](https://rocketchat.ow2.org/group/QAI-Town-Square).
	
2. Huawei MiniApp experts will review challenge propositions and inform the proposing party of acceptance or not.

3. Validated challenges
   - Huawei will initialise the challenge description and goals on the Telanto platform **in collaboration** with the proposing party/parties.
   - **Once everyone agrees** with the text, the challenge will be published on the Telanto platform (ie. made visible to universities).
   - If the challenge is accepted by a student team, it will be **run jointly run by Huawei and the relevant QAI participant(s)**.
   - When the challenge closes, its outputs will be published on the QAI portal (so long as the relevant licenses are visible - see _nota bene_ above).
   - The proposing party/parties, including the supervising professor, will be invited to write a blog post about their thoughts and experience concerning the challenge.
