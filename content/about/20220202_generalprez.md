---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Yes, we have a public presentation for you
subtitle: Something to share with colleagues and contacts
section: About
date: 2022-02-02
bigimg: [{src: "/img/posts/2021/coding.png", desc: "Coding (Sora Shimazaki, Pexels)"}]
comments: false
---

Interested by Quick Apps and the initiative? Need something offline to share with your boss, your colleagues, or contacts?
We've got you covered!

<!--more-->

Please find here the link to a general slide presentation that brings together the key elements and opportunities in a short, easily digestible document.

- [General presentation](/docs/OW2-Quick-App-Initiative-General-Presentation.pdf)

If you would like us to present this to your team or community, please contact us: [quickapp-team@ow2.org](mailto:quickapp-team@ow2.org?subject=I%20have%20a%20question%20about%20QAI)

