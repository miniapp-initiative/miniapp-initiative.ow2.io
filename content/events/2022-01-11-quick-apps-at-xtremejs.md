---
type: post             # Don't worry, it indicates the layout
excerpt_separator: <!--more-->   
                       # Use <!--more--> to create a summary  
highlight: true        # true if you want this item on the homepage 
draft: false             # true to avoid listing the article
title: Quick Apps at XtremeJS        # Main title of the article
subtitle: Sparking interest among developers  # Subtitle
date: 2022-01-11       # Date in ISO format
tags: [event,developers,xtremejs]      
bigimg: [{src: "/img/calendar.jpg", desc: "Calendar"}]
---

To wrap up 2021 on a great note, the Quick App initiative participated to [**XtremeJS**](https://xtremejs.dev/2021/), a professional online conference focused on **JavaScript**. XtremeJS took place on the 23rd of December and included **14 short sessions** with about **100 participants from all over the world**.




<!--more-->

[The conference talks](https://xtremejs.dev/2021/schedule/) covered various cutting-edge, new and sophisticated topics, such as **touchless apps**, **React components**, **Micro-Frontends**, **AI** and more.  

### “Quick Apps: Minimum Time-To-Market, Maximum UX”
Martin Alvarez Espinar, on behalf of Quick App Initiative, presented **Quick Apps** as a platform for hybrid mobile application development, based on the front-end web technology stack (JavaScript, CSS, and HTML) and the MVVM architecture. Martin explained how developers can create “light” applications efficiently, using built-in components, with just 20% of code compared to Android apps. 

On top of that, attendees learnt about the benefits of Quick Apps, like services and APIs for product lifecycle management, minimum time-to-market, promotion, user acquisition, monetization, user retention, etc.

Quick Apps certainly sparked interest among participants, as there were several questions raised by the audience. Given the time limit for each topic, further discussions were encouraged to take place via a dedicated discussion forum and a private group on LinkedIn for all participants and speakers.

### Wish you had been there?
No worries. All the sessions were captured on video and will be available to watch later. We will keep you posted!

### Would you like to learn more about Quick Apps? 

[Join Quick App Initiative](https://www.ow2.org/view/QuickApp/Participants_Form) and sign up to [our newsletter](https://mail.ow2.org/wws/subscribe/quickapp?previous_action=info) to stay tuned with QAI news the upcoming events.
