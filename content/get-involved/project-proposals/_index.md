---
title: Project Proposals
subtitle: active proposals
section: Join In
date: 2022-01-27
comments: false
draft: false
---

The following collaborative projects have been proposed by QAI participants.

- If these interest you, [contact us](mailto:quickapp-team@ow2.org?subject=qai%20project%20proposal%20question).
- If you would like to propose a project, see the [proposal process](/page/proposal-process).
- If you need inspiration, check out some [project ideas](/pages/project-ideas).
