---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: QAI becomes MAI
subtitle: Expanding the scope from Quick Apps to MiniApps
author: Christian Paterson
date: 2023-11-19
tags: ["qai","mai"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
highlight: true
draft: true
comments: true
---

Yes it's true, QAI is now MAI! 

<!--more-->

During the [spring 2023 Steering Committee](/docs/meetings/2023-05-05-steerco/) it was proposed that the Quick App Initiative (QAI) scope be broadened to cover the full [W3C MiniApp Working Group](https://www.w3.org/2021/miniapps/) scope. This was agreed by unanimous vote in June, with the migration taking place this autumn. __“The OW2 *Quick App* Initiative is dead, long live the OW2 *MiniApp* Initiative”__.

This is really fantastic news.
- It enables initiative participants to collaborate on the full range of W3C MiniApp technologies, from Mini Programs to Quick Apps and more.
- It aligns the initiative with the full W3C MiniApp Working Group scope, and encourages mutually supporting actions between the the initiative and the W3C.
- It positions MAI as the “go to place” for MiniApp experience sharing, use case exploration, code challenges, POCs and collaboration opportunities.

To mark the change, not only has the name changed from OW2 Quick App Initiative to OW2 MiniApp Initiative, but so to has the logo (thanks OW2 design people), and of course the charter and collaboration tooling.

As a bonus, we updated the participants list and now formally include e Foundation, ICOL Group, Startin’blox and Vonage. Nice!

Here's a quick run down of the actions done to make this change:
- Initiative website name and URL updated (https://miniapp-initiative.ow2.io/).
- OW2 collaboration space updated (https://gitlab.ow2.org/miniapp-initiative/miniapp-initiative).
- OW2 chat channel updated (https://rocketchat.ow2.org/group/MAI-Town-Square).
- OW2 website updated (https://www.ow2.org/view/miniapp/).
- GitHub QAI space archived (https://github.com/ow2-quick-app-initiative).
- GitHub MAI space created (https://github.com/OW2-MiniApp-Initiative).

See you all in 2024 for lots more OW2 MiniApp Initiative action.