---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
title: Web technologies for applications event
subtitle: Paris, June 13th
date: 2023-05-05
tags: [w3c,systematic,ow2,event]
author: Christian Paterson
bigimg: [{src: "/img/posts/2023/web_tech_event/QuickApp_website.png", desc: "web technologies event image"}]
comments: true
draft: false
---

A unique event looking at the intersection of _web innovation_, _emerging standards for light app technologies_ (such as MiniApps), and _mobile devices_.

<!--more-->

Access to the internet, and by extension the web, is a key enabler of human rights, citizen inclusion, knowledge sharing, education, commerce and even government. There are now nearly 2 billion web sites globally, and eCommerce generates hundreds of billions of dollars each year. Having a web presence is no longer a question, it’s a necessary requirement for today’s digital world.
 
Since the mid 2000’s there has been an exponential increase in web access from mobile devices fuelled by the rise of smartphones and increasingly affordable, high speed mobile data networks. Mobile native applications have raised the bar for what people expect when interacting with their devices; ubiquitous connectivity, fluid interactivity, rich functionality, and importantly, ease of app acquisition and use.
 
The shift towards an always on, always connected real-time society, combined with new technologies such as smart homes, smart cities, AR/VR, AI bots, telemedicine and telepresence challenges us. It challenges us to evolve existing global standards and create new ones. It challenges us to combat concerns about data privacy, cyber security, online harassment and more. It challenges us to keep the development process easy and accessible.
 
This 1 day event, **endorsed by the [W3C](https://www.w3.org/) (developers of global web standards), [Systematic](https://systematic-paris-region.org/?lang=en) (the largest innovation pole in France), and [OW2](https://www.ow2.org/) (Europe’s historic open source foundation)** will provide insight into web technology trends.
 
It will give you a rare opportunity to speak with renowned web experts not just from W3C, but also from industry.

<!--
**Dominique Hazaël-Massieux (W3C):** *"Landscape of Web Technologies for Apps"*
>Web Technologies are naturally at the core of Web applications running in browsers (including Progressive Web Apps), but they also form a critical platform for other application ecosystems: the usage of WebViews make them prevalent in many native apps on mobile and desktop alike, and they underpin the MiniApps ecosystem and its 1.4 billion active users. In this presentation, Dom will review and compare how Web technologies are used across these ecosystems, and highlight opportunities for greater convergence among them.

**Martin Alvarez-Espinar (Huawei):** *"W3C MiniApp Ecosystem"*
>MiniApps are light applications designed to perform specific functions within a super-app or platform. These apps are becoming increasingly popular due to their ability to simplify complex processes and improve user experience with seamless access and interaction. MiniApps are quickly becoming a must-have feature for any modern platform. The W3C MiniApp Working Group is developing standards and guidelines for creating and implementing MiniApps across multiple platforms and devices. This talk will present the background and current status of the W3C MiniApp specifications, developed collaboratively by industry leaders and stakeholders within the W3C MiniApp Working Group, to shape the future of mobile and web-based applications.

**Alex Bourlier (Startin'blox):** *"Using Solid to build a data protecting MiniApp ecosystem"*
>

**Simon Phipps (OSI):**
>

-->

And to top it all, this web technologies event is followed by OW2’s fantastic [annual conference](https://www.ow2con.org/view/2023/) in on June 14th and 15th ... **3 days of pure MiniApp and open source excellence in lovely Paris**

Make sure you reserve your free participation to these events soon; space is limited.
- **Register for the [morning industry workshop ](https://my.weezevent.com/web-technologies-for-applications-workshop-coding-contest)**


See you all soon!
