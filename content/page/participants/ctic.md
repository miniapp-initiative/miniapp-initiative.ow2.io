---
title: CTIC
subtitle: QAI Launch Participant
section: about
excerpt_separator: <!--more-->
date: 2021-12-10
bigimg: [{src: "/img/posts/2021/smart-ge0a9d5131_1920.jpg", desc: "Image by Tumisu from Pixabay"}]
comments: false
breadcrumbs: true
---

<!-- LOGO HERE -->
[![CTIC logo](/img/participant-logos/ctic.png)](https://www.fundacionctic.org/)

CTIC is a research and innovation center working on smart territories, active aging and well-being, and industrial digital transformation, interested in exploring new Quick App use.

<!--more-->

<!-- additional text here -->


<!-- why be part of QAI, or do you lead a Task Force or Project, etc. -->


<!-- Quick App showcase (if pertinent): no more than 2 apps -->
<!-- Write a short introduction to your 1 or 2 apps. Explain why you showcase them -->
<!-- For each app provide image, name, where people find it,  link to YouTube video if available + 1 sentence to explain why it is interesting -->
<!-- Above all, BE REASONABLE, this is not a marketing website ! -->

<!-- Standardized organization information -->
## Details
 
Country of incorporation/registration: Spain

Type of organization: <!-- choose from: public service, non-profit, SME, large enterprise, multi-national, other (specify) -->

Domain(s) of activity: <!-- short list/keywords here -->

Website: https://www.fundacionctic.org/

QAI representative: Pablo Coca

<!-- any other contact information that you would like to show -->
