---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
highlight: true
draft: false
title: What is a Quick App?
subtitle: Differences between Quick, native, and Web apps
author: Martin Alvarez
date: 2022-06-24
tags: ["technical"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
comments: true
---

Quick App is a new concept of light, native-like mobile applications, or _mini-apps_, that do not require installation and run directly on the operating system, offering high performance and a rich user experience. The Quick App concept is different to both native mobile apps and web apps, yet it also complements these other approaches perfectly. 

<!--more-->

Quick App is an Android application development framework based on emerging [W3C MiniApps standards](https://www.w3.org/groups/wg/miniapps/publications). From a developer perspective, it relies on the widely understood __front-end web technology stack__, including an HTML-like markup language, JavaScript, and CSS stylesheets. Quick Apps are light applications with access to the __device's native resources__, like advanced APIs (e.g., Bluetooth, system volume, or access to SMS). The close integration of the framework runtime with the device enables applications to use services at the operating system level. It is estimated that a Quick App requires only a fifth of the software code lines compared to its functionally equivalent native Android app. 

## Quick apps and Progressive Web Applications

The [W3C MiniApp Working Group](https://www.w3.org/groups/wg/miniapps) works on converging web-based light app paradigms, whilst fostering compatibility with the existing Web standards such as Progressive Web Applications (PWA). One example of this convergence is the extension of the [Web Manifest](https://www.w3.org/TR/appmanifest/), an essential part of PWA, to be used as a configuration and description file in MiniApps.

The __execution environment__ is perhaps the most significant difference between Quick App and PWA. While a MiniApp is bound to a specific platform (operating system) or super app, PWAs run in compatible web-enabled browsers.

Another essential difference is the __distribution mechanisms__. The resources of PWAs are distributed across the web, requiring a web server. On the other hand, Quick Apps are self-contained within a ZIP-based container that can be distributed on the web, listed on app marketplaces, or shared offline.

Both technologies use similar __programming languages__ and CSS-based stylesheets in terms of coding. The main difference is that Quick App implements a dedicated domain-specific language based on an HTML subset, providing a specific mechanism for data binding and event management. Quick Apps also implements the MVVM architecture with Virtual DOM management for more immediate user interaction. 

## Quick Apps and native applications

As set out, Quick Apps have access to a device's native resources similarly to native apps. The Quick App environment supervises the security aspects and manages users' permissions to protect their privacy as in native apps.

Quick Apps may be distributed as native apps. Both __packaging formats are similar__, using ZIP compression. 

The main difference is the __size__, __structure__, and __coding language__. 

Quick Apps are limited to a few megabytes (less than 4 MB). Native apps don't have these limitations, but on the flip side, reduced package sizes increase runtime agility and encourage rapid user interaction without a preceding and lengthy installation process. Let's compare two scenarios:

1. A user finds a native app in an app marketplace
- They must select to _download and install_ it before they can use it,
- Then wait for the download and install process,
- Then they need to find the app on their device before finally selecting it to run.

2. A user finds the equivalent app deployed as a Quick App
- They select to _download and run_ it ... in just one click!
- The system performs background caching and _immediately_ launches the Quick App in just seconds.
- No waiting, no hunting on home-screens, no hoops to jump through.

Due to this inherent speed from see-to-do, publishers realize an increase engagement rate and of course, always have the option of using the Quick App as a mechanism to attract users towards native apps where appropriate.

Finally, Quick Apps are based on pages and components (similar to web pages and reusable Web Components). In contrast, native apps are designed around on complex structures and programming languages. 

![Quick Apps structure: components and pages](/img/posts/2022/quickapp-pages-components.svg)

## Summary of differences

The following table summarizes the differences between the three app dynamics.  

| Feature | Quick App | PWA | Native App |
|---------|:---------:|:---:|:----------:|
| Native-like (styles, full screen…) | YES | YES | YES | 
| Open app with web link (QR Code) | Partial | YES | NO |
| Ideal for on-the-move scenarios (quick interaction) | YES | YES | NO |
| Native performance (quick processing, rendering) | NO | NO | YES |
| Low code (easy to develop) | YES | YES | NO |
| Automatic update (no user interaction needed) | YES | YES | NO |
| Discoverability (marketplace, global search…) | YES | YES | YES |
| Widget on home-screen | Partial | NO | NO |
| Icon on the home-screen | YES | YES | YES |
| Standalone (no server needed) | YES | NO | YES |
| Access to system APIs | YES | Partial (Chromium supports) | YES |
| No marketplace needed | YES | YES | YES |
| Coding: UI predefined elements | YES | YES | YES |
| Push notifications | YES | YES | YES |
| Promotion within devices (e.g., device's assistant) | YES | YES | YES |
| Deep links | YES | YES | YES |
| App in AppGallery | YES | YES | YES |
| Monetization (ads, iAP, rewarded ad videos…) | YES | YES | YES |
| Cache for offline operation | YES | YES | YES |
| Multi-lingual | YES | YES | YES |


These three app dynamics all have pros and cons depending on the usage scenario, and all of them are compatible and can coexist happily in mobile ecosystems.

Quick Apps and PWA are ideal for instant user interaction. Quick App makes the most of the device and the platform, like native but for use cases requiring less complicated functions. Quick App platform vendors can also bring additional opportunities for publishers to promote their _light_ apps through channels like marketplaces, recommended apps, in-device widgets, etc. 

Case by case, developers and publishers should evaluate each technology's possibilities and opportunities for their specific scenarios. If you are in doubt, [let us know](mailto:quickapp-team@ow2.org), and we can help you decide.
