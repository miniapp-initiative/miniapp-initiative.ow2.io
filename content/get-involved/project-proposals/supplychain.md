---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Supply chain & sustainability
subtitle: Collaborative project proposal
section: Join In
date: 2022-01-27
comments: false
draft: false
---

Alliance Tech propose to explore how Quick Apps can be leveraged within end-to-end supply chains as a mechanism to increase product customization potentials, facilitate supply side marketplaces (especially for SMEs), raise supply chain efficiencies, provide end-to-end data points to help deliver new services and revenue opportunities. A priority outcome should be to show how the use of Quick Apps along a supply chain can help support stakeholder goals concerning sustainability.

<!--more--> 

### Participants
- Lead: Alliance Tech (France)
- Additional: ICOL Group (Italy)

### Aims
- A 3 month **opportunity study**.
- Focus on the apparel and footwear industry in order to identify concrete opportunities for the sectoral adoption of Quick Apps.

### Requested assistance
- Sponsorship funding: 51K€

### Status
- Project accepted by the Steering Committee.
- Sponsors being sought.

### Read more
- [Project proposal slides](/project-proposals/AllianceTech_proposal_call4assistance.pdf).
