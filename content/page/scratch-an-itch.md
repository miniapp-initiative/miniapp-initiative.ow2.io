---
title: Quick Apps scratch an itch
subtitle: Do we need a new app dynamic?
section: About
date: 2022-01-27
comments: false
---

Since the dot-com boom of the late 90s, our relationship with the world around us has been evolving into a hybrid, sometimes chimeric,  hyper connected, instant gratification, online/offline amalgam. As with any cultural and technological transition, this has happened through serendipity as well as inspired foresight. It has spawned new industries, disrupted others, changed forever how we learn, work, communicate, consume media, pay bills, book holidays, shop, interact with government services, make friends, gather & share news, plan routes, play games, order coffee, use alternative transport.

Yet, it's difficult to imagine how the mobile app dynamic in its current incarnation, with its current actors, can efficiently, sustainably and inclusively continue to scale with this transition. Not just in relation to the above, but also in anticipation of rapidly emerging trends in smart homes, smart cities, smart transport and smart health, or in relation to societal shifts towards increased “on-the-go” activity and reduced “contact” living.

Here are a few "itches" that we believe that Quick Apps scratch:<br>
(you can read more "itches" in the [editorials section](/editorials/))

## Companies and public services

Increasingly organisations, whether they be for-profit, non-profit, academic or govermental, need an app presence in order to reach their audiences, yet developing, publishing and updating apps is an impractical proposition for many. A lack of app presence robs organisations of the rich engagement and business opportunities that they provide.

## Communities
  
In many _scenarios_ publishing apps through online marketplaces is simply not realistic. Not just from an organisational perspective (who does it, how is it maintained, who's the contact point, etc. etc.), but also from a user perspective. Here are a few examples:
  - part of school courses ("hey kids, this term we're going to make an app for your phone"), 
  - maker communities and home hobbyist projects,
  - local community events and sporting meets,
  - local bake sales,
  - student research surveys and experiments.
  - ...

## App marketplaces

Both a blessing and a curse, the online app marketplaces have helped usher in the wonderful subject of mobile productivity, media consumption, commerce, entertainment and social connectivity, yet they are not without their negatives.
- They are filled by millions of new apps each year, making it paradoxically harder for users to find apps, and frustratingly harder for businesses to get their apps seen;
- As both a direct and indirect result of business acumen, market power and shareholder sensitivity, marketplace owners have become self-appointed gatekeepers that decide censorship rules, skim usage taxes, hoover up vast quantities of data, all the whilst stifling *open* market cost dynamics, and absolutely controlling in-store publication and promotion opportunities. 

## Sustainability

We live on a planet that has finite resources and a growing list of ecological issues. Consumers demand greater action to tackle these issues, and companies are always looking to maximise supply chain efficiency as a means to increase profits and increasingly presevre resources. Yet for all this, we continue to increase the volume of apps we download, leading to device bloat and adding one more reason to upgrade. And let's not forget that access to devices with higher memory capacities is not at all in the reach of everyone.

As a no-install dynamic that favours reduced device impact, Quick Apps can surely play a part in helping us connect to our increasingly digital world, whilst respecting that device storage can't keep increasing.

If you are interested by the subject of Quick Apps and supply chain efficiency and sustainability, check out our [project proposals](/get-involved/project-proposals/).

## Choice overload

On a final note, study after study highlight the growing problems of *user app fatigue*, especially for _ad hoc_ needs. Users are tired of hunting for apps in the online app marketplaces; there are simply *too many* choices and possibilities. They are tired of having to download an app for every new connected object, or online experience they want use, and they are tired of downloading apps for ad hoc usage (example, to access a hotel car park).

