---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: OW2 and W3C liaison
subtitle: Supporting MiniApp and web technology
author: Christian Paterson
date: 2023-09-21
tags: ["w3c","mai","ow2"]
bigimg: [{src: "/img/mockups.jpg", desc: "MiniApps"}]
highlight: true
draft: true
comments: true
---

title: OW2 and W3C liaison
subtitle: Supporting MiniApps, web technologies and open source in Europe
author: Christian Paterson
date: 2023-09-21
 
It's taken some time getting there, but the W3C and OW2 have agreed a liaison to foster opportunities for co-supported MiniApp and Web Technology actions. See the [W3C liaison page](https://www.w3.org/liaisons/#ow2).

<!--more-->
 
This is fantastic news. The W3C has been a great supporter of MiniApps, and kindly invested in helping to spread MiniApp awareness. For example, the MiniApp [workshops in Spain](https://chapters.w3.org/hispano/event/tecnologias-web-para-desarrollar-aplicaciones-moviles/) earlier this summer in collaboration with the University of Oviedo, the City of Gijon and CTIC, the [W3C Spain Chapter](https://chapters.w3.org/hispano/). As an aside, CTIC was also one of the launch [participants](/docs/participants/ctic/) of the Quick App Initiative.
 
Furthermore, the liaison is also a nice recognition of [OW2](https://www.ow2.org/), its position within the European open source ecosystem, and of course its Quick App Initiative.
 
Both groups are highly complementary; the [W3C MiniApps Working Group](https://www.w3.org/2021/miniapps/) is focused on technical standardization, whilst the OW2 and its _Quick App_ Initiative (since reborn as the _MiniApp_ Initiative) is focused on experience sharing, use case exploration, collaborative development and outreach.
 
> "OW2 is happy to team with the W3C to support MiniApps, appreciates the dynamism and openness of the Quick App initiative, and hopes to play a lead role in fostering the emergence of open source MiniApp reference implementations along with associated tools, technologies and showcase open source projects." (Pierre-Yves Gibello, OW2 CEO)
