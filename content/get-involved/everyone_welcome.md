---
type: post
excerpt_separator: <!--more--> 
# Do not remove 'type: post'
# Use <!-- more --> as a mark to delimitate the excerpt of the article  
# highlight=true if you want this item to be listed on the homepage 
title: Everyone is welcome
subtitle: 
section: Join In
date: 2022-01-27
comments: false
draft: false
---

The Quick App Initiative is about creating a vibrant, dynamic, multi-cultural community of people, companies and organisations that are motivated to share experiences, learn from each other, make some cool Quick Apps (and hopefully some open source), foster a new Quick App dynamic in Europe and beyond.

<!--more--> 

Participation to the initiative is not through pay-to-play, or in accordance to selection criteria. You do not need to ask to be a participant, nor do you need to ask if you would like to "co-opt" or invite a contact. Simply sign up [here](https://www.ow2.org/view/QuickApp/Participants_Form).

### Open to all

- **Individuals** regardless of culture, race, relgion, gender identity, or other:
  - Developers (professionals, hobbyists, students) interested in Web and native app technologies;
  - Communication experts who would like to help us reach a larger audience;
  - Business development experts who see potential in this new paradigm;
  - ...

- **Companies** regardless of country of registration or domain of activity:
  - OEMs and operating system providers interested in _supporting_ Quick Apps;
  - Content and service providers interested in _making_ Quick Apps;
  - Marketing companies looking to produce marketing campaigns that fusion on and offline;
  - Innovative entrepreneurs and SMEs.
  - ...

- **Non profits**
  - Municipalities interested in making their services easily accessible for citizens and visitors;
  - Research centers and academic institutions interested using Quick Apps as an easy POC, test and distribution mechanism;
  - NGOs
  - ...

### Respectful

In counterparty to this openness, there are a couple of provisos:
1. Actions of the initiative are managed by the Steering Committee in order to uphold (as possible) the social, professional, commercial and moral standing of its participants. In short, anyone can join, but not every suggestion or action will be agreed.
2. **All participants in the Quick App Initiative agree to be bound by:**<br>
  - The [QAI charter](/docs/charter.pdf);<br>
  - The [OW2 bylaws and policies](https://www.ow2.org/view/Membership_Joining/Legal_Resources);<br>
  - The [OW2 Code of Conduct](https://www.ow2.org/view/Membership_Joining/Code_of_Conduct).

To join QAI, please register here: https://www.ow2.org/view/QuickApp/Participants_Form

