---
type: post
# Do not remove 'type: post'
highlight: false
# highlight=true if you want this item to be listed on the homepage 
title: OW2Con'21
subtitle: Leveraging the European Open Source Ecosystem
author: Christian Paterson
date: 2021-06-03
tags: [ow2,ow2con,event]
bigimg: [{src: "/img/calendar.jpg", desc: "Calendar"}]

---


OW2Con'21 is the annual event organized by [OW2](https://www.ow2.org). This conference is free of charge and open to anyone and it attracts international experts to discuss about open source software in modern corporate information systems.

During one of the Ow2Con'21 keynotes, the Quick App Initiative was publicly announced.  

- **When**: 23-24 June 2021
- **Where**: [online](https://www.ow2con.org/view/2021/)

<!--more-->

OW2con’21 addressed the many challenges of ensuring a sustainable European open source ecosystem.

Useful links:
<!-- - [Registration](https://www.ow2con.org/view/2021/RegisterOW2online?year=2021&event=OW2con21) -->
- [Program](https://www.ow2con.org/view/2021/Program?year=2021&event=OW2con21)
- Event [website](https://www.ow2con.org/view/2021/OW2online_Intro?year=2021&event=OW2con21)



