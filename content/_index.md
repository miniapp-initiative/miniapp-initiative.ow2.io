**MiniApps** are a class of _low-code_, _no-install_ applications defined by [W3C MiniApp standards](https://www.w3.org/2021/miniapps/) MiniApp implementations (such as Quick Apps) offer consumers and developer **[multiple benefits](editorials/2021-09-15-quick-apps-speedy-services-less-coding-effort/)**.

This [OW2](https://www.ow2.org/) **open forum** aims to support the MiniApp ecosystem in **Europe** through awareness raising, use case exploration, and development collaboration. It is a place to share experiences and provide _grassroots feedback_ into MiniApp standards.

{{< rawhtml >}}
<table width="80%" cellpadding="0" cellspacing="0">
    <tr style="border:0px;">
        <td style="border:0px;background-color:#F5F5F5;">- <a href="/docs/charter.pdf">Charter</a></td>
        <td style="border:0px;background-color:#F5F5F5;">&nbsp;</td>
        <td style="border:0px;background-color:#F5F5F5;">- <a href="/developers">Developer documentation</a>
    </tr>
    <tr style="border:0px;">
        <td style="border:0px;background-color:#F5F5F5;">- <a href="/about/20220106_aims/">Aims</a></td>
        <td style="border:0px;background-color:#F5F5F5;">&nbsp;</td>
        <td style="border:0px;background-color:#F5F5F5;">- <a href="https://github.com/ow2-miniapp-initiative/">GitHub space</a>
    </tr>
    <tr style="border:0px;">
        <td style="border:0px;background-color:#F5F5F5;">- <a href="/page/participants">Participants</a></td>
        <td style="border:0px;background-color:#F5F5F5;">&nbsp;</td>
        <td style="border:0px;background-color:#F5F5F5;"><i><a href="mailto:miniapp-team@ow2.org?subject=I%20have%20a%20question%20about%20MAI">... ask a question</a></i></td>
    </tr>
</table>
<br>
{{< /rawhtml >}}

<!--
- [Founding charter](https://miniapp-initiative.ow2.io/docs/charter.pdf)
- [Initiative aims](/about/20220106_aims/)
- [Initiative participants](/page/participants)
- [Developer documentation](/developers)
-->

{{< bigbutton label="Join Now" link="https://www.ow2.org/view/miniapp/Participants_Form" >}}
