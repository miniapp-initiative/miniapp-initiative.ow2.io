---
type: post
title: Steering Committee no.4
subtitle: February 9th 2022
author: Christian Paterson
date: 2022-02-17
highlight: true               # true or false -> true will be shown on the homepage 
tags: ["steerco"]
bigimg: [{src: "/img/1308819_round-table.jpg", desc: "Round-table discussion"}]
draft: false
---

Please find below links to the support slides and minutes from our 4th QAI Steering Committee (SteerCo) held 9th February 2022.


If you have any comments, thoughts, or suggestions please contact us on the mailing list (quickapp@ow2.org), or via [chat](https://rocketchat.ow2.org/group/QAI-Town-Square). You can also create an issue within [GitLab](https://gitlab.ow2.org/quick-app-initiative/quick-app-initiative/-/boards).

---

- [Support slides](/docs/SteerCos/20220209_qai_steerco/20220209_QAI_SteerCo_support.pdf "QAI SteerCo 2022-02-09 Slides")
- [Minutes](/docs/SteerCos/20220209_qai_steerco/20220209_QAI_SteerCo_MoM.pdf "QAI SteerCo 2022-02-09 Minutes")

---

And don't forget, the **next SteerCo is on April 13th <INS>2022</INS>, 10am CET**.

Many thanks for your support and motivation. See you all soon.

Stay safe and have a great 2022
<br>The QAI team
