---
title: Academia Challenges
subtitle: Student led innovation
section: Join In
date: 2023-01-10
comments: false
draft: false
---

Huawei has teamed with Telanto to organize student challenges, and have kindly invited the QAI community to propose ideas for *co-animated* challenges.

Challenges can take several forms, target students at different education levels, in different regions, and on different courses. It's all very flexible - read more [here](/get-involved/challenges/whats_a_challenge/).

Why not propose a challenge [today](/get-involved/challenges/challenge_process_and_terms/) - **we look forward to seeing _your_ ideas!**
